# README #

How to build and run

### Build ###

To build a image from Docker file, use this command **docker build -t descricao:v1 .** in the command line.

### Run ###

To run a image, use this command **docker run -it descricao:v1** in the command line.

